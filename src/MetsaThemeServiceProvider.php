<?php namespace Visiosoft\MetsaTheme;

use Anomaly\Streams\Platform\Addon\AddonCollection;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;


class MetsaThemeServiceProvider extends AddonServiceProvider
{

    protected $overrides = [
        'streams::errors/404' => 'theme::errors/404',
        'streams::errors/500' => 'theme::errors/500',
    ];

    protected $routes = [
        'posts/count/view/{id}'                               => [
            'as'   => 'anomaly.module.posts::posts.view_counter',
            'uses' => 'Visiosoft\MetsaTheme\Http\Controller\PostsController@view_counter',
        ],
    ];

    public function boot(AddonCollection $addonCollection)
    {
        $slug = 'news_settings';
        $section = [
            'title'   => 'Haber Ayarları',
            'href'    => '/admin/settings/themes/visiosoft.theme.metsa',
        ];
        $addonCollection->get('anomaly.module.settings')->addSection($slug, $section);
    }
}
