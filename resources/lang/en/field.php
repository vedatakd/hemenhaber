<?php

return [
    'recent-news' => [
        'name' => 'Son Dakika Haberleri'
    ],
    'featured-news' => [
        'name' => 'Öne Çıkan Haberler'
    ],
];
