<?php

return [
    'web-logo' => [
        'name' => 'Web Logo',
        'warning' => '<b>193x45 pixels</b>',
    ],
    'mobile-logo' => [
        'name' => 'Mobile Logo',
        'warning' => '<b>193x45 pixels</b>',
    ],
    'site-description' => [
        'name' => 'Site Footer Description',
    ],
    'site-address' => [
        'name' => 'Site Footer Address',
    ],
    'site-support-phone' => [
        'name' => 'Site Footer Support Phone',
    ],
    'header-banner-type' => [
        'name' => 'Header Banner Type',
    ],
    'header-banner-code' => [
        'name' => 'Header Banner Kodu',
    ],
    'header-banner-image' => [
        'name' => 'Header Banner Image',
        'warning' => '<b>734x90 pixels</b>',
    ],
    'content-banner-type' => [
        'name' => 'Content Banner Type',
    ],
    'content-banner-code' => [
        'name' => 'Content Banner Kodu',
    ],
    'content-banner-image' => [
        'name' => 'Content Banner Image',
        'warning' => '<b>1173x282 pixels</b>',
    ],






    'home-sidebar-banner-one-type' => [
        'name' => 'Home Sidebar Banner One Type',
    ],
    'home-sidebar-banner-one-code' => [
        'name' => 'Home Sidebar Banner One Code',
    ],
    'home-sidebar-banner-one-image' => [
        'name' => 'Home Sidebar Banner One Image',
        'warning' => '<b>370x451 pixels</b>',
    ],



    'detail-sidebar-banner-one-type' => [
        'name' => 'Detail Sidebar Banner One Type',
    ],
    'detail-sidebar-banner-one-code' => [
        'name' => 'Detail Sidebar Banner One Code',
    ],
    'detail-sidebar-banner-one-image' => [
        'name' => 'Detail Sidebar Banner One Image',
        'warning' => '<b>370x451 pixels</b>',
    ],
    'detail-sidebar-banner-two-type' => [
        'name' => 'Detail Sidebar Banner Two Type',
    ],
    'detail-sidebar-banner-two-code' => [
        'name' => 'Detail Sidebar Banner Two Code',
    ],
    'detail-sidebar-banner-two-image' => [
        'name' => 'Detail Sidebar Banner Two Image',
        'warning' => '<b>370x272 pixels</b>',
    ],



    'categories-sidebar-banner-one-type' => [
        'name' => 'Categories Sidebar Banner One Type',
    ],
    'categories-sidebar-banner-one-code' => [
        'name' => 'Categories Sidebar Banner One Code',
    ],
    'categories-sidebar-banner-one-image' => [
        'name' => 'Categories Sidebar Banner One Image',
        'warning' => '<b>370x451 pixels</b>',
    ],
    'categories-sidebar-banner-two-type' => [
        'name' => 'Categories Sidebar Banner Two Type',
    ],
    'categories-sidebar-banner-two-code' => [
        'name' => 'Categories Sidebar Banner Two Code',
    ],
    'categories-sidebar-banner-two-image' => [
        'name' => 'Categories Sidebar Banner Two Image',
        'warning' => '<b>370x272 pixels</b>',
    ],












    'navigation-categories' => [
        'name' => 'Navigation Categories'
    ],
    'headline-top-left' => [
        'name' => 'Home Page Top Left HeadLine'
    ],
    'headline-bottom-left' => [
        'name' => 'Home Page Bottom Left HeadLine'
    ],
    'headline-top-right' => [
        'name' => 'Home Page Top Right HeadLine'
    ],
    'headline-bottom-right' => [
        'name' => 'Home Page Bottom Right HeadLine'
    ],
    'breaking-news-type' => [
        'name' => 'Breaking News Type'
    ],
    'headline-center-type' => [
        'name' => 'HeadLine Center Type'
    ],
    'popular-news-slider' => [
        'name' => 'Popular News Slider'
    ],
    'featured-category-section' => [
        'name' => 'Featured Section Categories'
    ],
    'bottom-section-one-column-one' => [
        'name' => 'Bottom Section One Column One'
    ],
    'bottom-section-two-categories' => [
        'name' => 'Bottom section Two Categories'
    ],
    'youtube-api-key' => [
        'name' => 'Youtube Playlist API Key',
    ],
    'youtube-playlist-code' => [
        'name' => 'Youtube Playlist Code',
        'warning' => 'https://www.youtube.com/playlist?list=<b>xxxxxxxxxxxxxxxxxxxxxxxxxxxxx</b>',
        'placeholder' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    ],
    'footer-news-block-one' => [
        'name' => 'Footer News Block One'
    ],
    'footer-news-block-two' => [
        'name' => 'Footer News Block Two'
    ],
    'footer-news-block-three' => [
        'name' => 'Footer News Block Three'
    ],
    'bottom-section-two-column-name' => [
        'name' => 'Bottom Section Two Column Name'
    ],
    'bottom-section-one-column-one-name' => [
        'name' => 'Bottom Section One Column Name'
    ],
    'twitter-feed' => [
        'name' => 'Twitter Feed Address'
    ],
    'facebook-url' => [
        'name' => 'Facebook Address'
    ],
    'twitter-url' => [
        'name' => 'Twitter Address'
    ],
    'instagram-url' => [
        'name' => 'Instagram Address'
    ],
    'youtube-url' => [
        'name' => 'Youtube Address'
    ],
];
