<?php

use Anomaly\PostsModule\Category\CategoryModel;
use Visiosoft\MetsaTheme\Handler\Categories;

return [
    "web-logo" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'default_value' => "",
            'max' => '2',
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png']
        ]
    ],
    "mobile-logo" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png']
        ]
    ],

    "site-description" => [
        'type' => 'anomaly.field_type.text',
        "config" => [
            'default_value' => 'Lorem ipsum dolor sit amet, consectet adipiscing Se velit ex, dictum at nunc placerat consequatS quam. ornaremi condiment PhasellusI cursii placerat quam et, mattis nibh Suspendislacinias.'
        ]
    ],
    "site-address" => [
        'type' => 'anomaly.field_type.text',
        "config" => [
            'default_value' => "House No 08, Din Bari, Dhaka, Bangladesh",
        ]
    ],
    "site-support-phone" => [
        'type' => 'anomaly.field_type.text',
        "config" => [
            'default_value' => "(+660 256 24857) , (+660 256 24857)",
        ]
    ],

    'header-banner-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],

    "header-banner-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "header-banner-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],

    'content-banner-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],

    "content-banner-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "content-banner-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],

    'home-sidebar-banner-one-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],

    "home-sidebar-banner-one-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "home-sidebar-banner-one-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],

    //detail sidebar

    'detail-sidebar-banner-one-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],

    "detail-sidebar-banner-one-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "detail-sidebar-banner-one-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],
    'detail-sidebar-banner-two-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],
    "detail-sidebar-banner-two-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "detail-sidebar-banner-two-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],
    //End detail sidebar
    //categories sidebar
    'categories-sidebar-banner-one-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],

    "categories-sidebar-banner-one-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "categories-sidebar-banner-one-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],
    'categories-sidebar-banner-two-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "image",
            'options' => [
                'image' => 'Resim',
                'code' => 'Editör Kodu',
            ],
        ],
    ],
    "categories-sidebar-banner-two-image" => [
        'type' => 'anomaly.field_type.file',
        "config" => [
            'folders' => ['images'],
            'max' => '2',
            'default_value' => "",
            'mode' => 'upload',
            'allowed_types' => ['jpeg', 'jpg', 'png', 'gif']
        ]
    ],
    "categories-sidebar-banner-two-code" => [
        'type' => 'anomaly.field_type.editor',
        "config" => [
            'default_value' => "",
            'mode' => 'twig',
            'word_wrap' => true,
        ]
    ],

    //End categories sidebar

    "navigation-categories" => [
        'type' => 'anomaly.field_type.checkboxes',
        "config" => [
            "handler" => Categories::class,
            'min' => 1,
            'default_value' => [5, 2, 3, 4],
        ]
    ],
    'headline-center-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "featured",
            'options' => [
                'featured' => 'Öne Çıkan Haberler',
                'recent' => 'Son Eklenen Haberler',
            ],
        ],
    ],
    'breaking-news-type' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'default_value' => "recent",
            'options' => [
                'featured' => 'Öne Çıkan Haberler',
                'recent' => 'Son Eklenen Haberler',
            ],
        ],
    ],
    'headline-top-left' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 6,
        ],
    ],
    'headline-bottom-left' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 5,
        ],
    ],
    'headline-top-right' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 4,
        ],
    ],
    'headline-bottom-right' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 3,
        ],
    ],
    'popular-news-slider' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 2,
        ],
    ],
    "featured-category-section" => [
        'type' => 'anomaly.field_type.checkboxes',
        "config" => [
            "handler" => Categories::class,
            'min' => 1,
            'default_value' => [2, 3, 4],
            'max' => 3,
        ]
    ],
    'bottom-section-one-column-one-name' => [
        'type' => 'anomaly.field_type.text',
        'config' => [
            'default_value' => "Gündemden Haberler",
        ],
    ],
    'bottom-section-one-column-one' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 2,
        ],
    ],
    'bottom-section-two-column-name' => [
        'type' => 'anomaly.field_type.text',
        'config' => [
            'default_value' => "Diğer Manşetler",
        ],
    ],
    "bottom-section-two-categories" => [
        'type' => 'anomaly.field_type.checkboxes',
        "config" => [
            "handler" => Categories::class,
            'min' => 1,
            'default_value' => [1, 2, 3, 4],
            'max' => 4,
        ]
    ],
    "youtube-api-key" => [
        'type' => 'anomaly.field_type.text',
        "config" => [
            'default_value' => 'AIzaSyB3fCjSfIPVxQKd4duKOeLTCBDksdN7qUI'
        ]
    ],
    "youtube-playlist-code" => [
        'type' => 'anomaly.field_type.text',
        "config" => [
            'default_value' => 'PLsmqeqKj7M-rx1GmqYA4THAa7oWj00DNU'
        ]
    ],
    'footer-news-block-one' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 2,
        ],
    ],
    'footer-news-block-two' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 2,
        ],
    ],
    'footer-news-block-three' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            'related' => CategoryModel::class,
            'default_value' => 2,
        ],
    ],
    "twitter-feed" => [
        'type' => 'anomaly.field_type.text',
        "config" => [
            'default_value' => 'ntvspor'
        ]
    ],
    "facebook-url" => [
        'type' => 'anomaly.field_type.url',
        "config" => [
            'default_value' => '#'
        ]
    ],
    "twitter-url" => [
        'type' => 'anomaly.field_type.url',
        "config" => [
            'default_value' => '#'
        ]
    ],
    "instagram-url" => [
        'type' => 'anomaly.field_type.url',
        "config" => [
            'default_value' => '#'
        ]
    ],
    "youtube-url" => [
        'type' => 'anomaly.field_type.url',
        "config" => [
            'default_value' => '#'
        ]
    ],
];
